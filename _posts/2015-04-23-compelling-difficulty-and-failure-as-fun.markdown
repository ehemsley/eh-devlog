---
layout: post
title:  "Compelling Difficulty and Failure as Fun"
date:   2015-04-23 16:45:00
category: Design
tags: [design, difficulty, roguelikes]
---

*Rogue* is all the rage these days.
Concepts like randomly-varied levels or permadeath, once considered suitable only for the hardcorest of the hardcore players, are
finding themselves slipping into mainstream game design. Features like this are enough to
give a game the "roguelike" stamp. The term, unfortunately, is now close to being diluted into near meaninglessness.
As a longtime fan of the genre, it irks me to see how this descriptor is now lazily applied to any game that appropriates
particular gameplay elements. I would like to paint a picture of the roguelike for you, in an attempt to elucidate what it
is about them that has fascinated players for decades, and why many games that call themselves roguelikes fall short
of the ideals of the genre.

Much like *Doom* which would appear a decade later, *Rogue* had an iconic design that spawned countless derivatives. These came to be known
as roguelikes. *Rogue* featured dungeons that were procedurally generated each time you started a game. Your goal was simply to retrieve
an item known as the Amulet of Yendor, which was located at 26 floors or below. It was brutally hard, and if you died, your progress was reset and you started back at level 1, with
completely different randomly generated levels. Beating the
game even once is a massive achievement. For those who love the genre,
these features make the game compelling, since one never knows quite what to expect and must
adaptively react to challenges as they arise. Some say these elements make the game unfairly difficult -
since the player doesn't have access to the same resources and encounters each time, their success is not guaranteed and may in fact be due to the whims of the random
number generator.

![Ahh, the simple joys of ASCII art](http://i.imgur.com/ML5QUm8.gif)

But are roguelikes, in the traditional sense, truly unfair? And what exactly is it that makes them so compelling?

Let's talk more specifically about the design of *Rogue*.

*Rogue* operates essentially as a turn-based game. Every time the player takes an action, every creature on the floor also takes an action. The player has an unlimited
amount of time to decide what to do on their turn.
The player can find and use equipment, namely weapons, armor, and rings. On any given turn the player can:

* Attack
* Move
* Rest (regains health)
* Search (reveals hidden doorways and traps)
* Use an item (scrolls, potions, etc.)

The various monsters you encounter in *Rogue* have many unique effects. There is an enemy type known as the Nymph. Nymphs aren't very dangerous combat-wise, but they have a nasty tendency to steal your unequipped items.
Vampires will drain your maximum HP when their attacks connect. And so on and so forth.
But, in the end, your biggest enemy in *Rogue* is lack of knowledge. As a beginner, you will die simply because you don't have enough experience to anticipate what is likely to occur.
For example, you might be tempted to equip the heaviest armor you can find. However, there is a common enemy type called the Aquator. Every time they hit you, you lose
1 armor point - UNLESS you have equipped leather armor. (The game does not explicitly give you this information, of course.)
If you use non-leather armor, you will often end up with low armor value after fighting many Aquators, which is extremely dangerous. Most high-level
Rogue players will often only use leather armor for their entire playthrough to avoid this situation. New players also have a tendency to run and fight monsters in the open,
leaving open the possibility of being overwhelmed by too many opponents, while expert players will abuse doorways to prevent having to fight more than one monster at at time.
To put it succinctly: Inexperienced players *react*. Experienced players *anticipate*.

In 1981, shortly after *Rogue* had begun to catch on (mostly at university computer science departments), some Carnegie-Mellon CS majors developed a
program known as *Rog-O-Matic*. Classified as an expert system, *Rog-O-Matic* was able to consistently attain higher scores than expert human *Rogue* players.
The program did not use memory hacks, so it worked under the same knowledge limitations as human players. And yet, in spite of the random nature of the game,
the program was able to consistently win games of *Rogue*. In other words... the programmers were able to recognize patterns that could be exploited.

This indicates that *Rogue* isn't so hard merely because of randomness - it's hard because it punishes you very severely for going into situations unprepared, or for losing your cool and making a stupid move.
It doesn't punish you for being unlucky, so much as it punishes you for not playing smart to prevent situations from getting out-of-hand.
There is a complexity of interaction in *Rogue* which enables the player to gain meaningful experience and react accordingly to situations as they arise.
The player must know when to use their special items, how to use the terrain to their advantage, and understand the many unique effects that monsters have.
Rogue isn't compelling because it's unfairly and randomly hard - it's compelling because it rewards your understanding of its lessons with higher rates of success.
Death in *Rogue* is a teachable moment. This is absolutely fundamental and crucial to its design.

Future roguelikes would push the complexity of interaction to higher levels.
Let's examine one of my all-time favorite roguelikes: *Shiren the Wanderer*. *Shiren* is a roguelike where each
procedurally-generated level belongs to a particular "zone". Each zone has a unique theme with different enemies and items.
Once you've cleared a certain amount of floors in the zone, you arrive in a village
containing shops where you can buy useful items, warehouses where you can store items for future use, and NPCs that you can interact with. *Shiren* was the first roguelike to include
elements that persist after you die. But in the grand scheme of things, the most important factor to your success is still your experience, and your ability to
patiently consider good moves, and avoid bad ones.

![shiren](http://i.imgur.com/6Uzo1Lz.jpg)

Here's a story from a StW player that I found on GameFAQs forums:

> I'm doing super crazy awesome. A monster house ONOZ! Luckily, it's one of those floors with only two rooms, and the other one is a shop. So when a Skull Mage warps me in there, I quickly grab an item to get the shopkeeper to block the entrance.
> I casually look over the goods, grab some stuff, and walk towards the entrance to pay for it. Meanwhile, every monster on the floor is waiting in line to kill on me when the shopkeeper moves.
> Guess who's at the head of the line? Guess who can cast switching magic? Guess who I'm diagonally aligned with? I am now outside the shop, carrying unpaid goods. The shopkeeper kills me.
> God d\*\*\* it.

Let's break down what is going on here. There are multiple rules in play.
1) The player is in a shop. 2) Shops always have shopkeepers. 3) Shopkeepers block the entrance to the store when you grab an item in the store.
4) Monsters will not attack shopkeepers. The player exploits this to avoid having to fight a large group of monsters at once. 5) Skull Mages are an enemy type which has a special ability: they can switch places with the player when their magic hits the player.
6) Monsters can fire magic and other projectiles diagonally. 7) Shopkeepers are very powerful and will turn aggressive if the player leaves a shop without paying for their items.

In this case, the player carelessly moved into the line of sight of a Skull Mage, a seemingly inconsequential event which ultimately caused them to die. Experiences like this are common when playing
*Shiren the Wanderer*. You must pay attention, even to trifles, or you fail.

But there's something else going on here beyond mere difficulty.
What we see in the above situation is a simple set of rules - but it is the
relations between these simple rules which produce behavior that is unexpected, complex, *and humorous*. In other words, *losing can be fun*.
I would argue that this is another crucial aspect of the appeal of roguelikes. If one is designing a game in which
the player will be defeated more often than not, it is imperative that the act of being defeated is not always the cause of hopeless frustration.
It can be amusing to fail! When failing is fun, then learning is fun!

Furthermore, the beauty of roguelikes is that progress is not measured by whether you're on level 2 or level 16 - it is measured by how much you have learned,
and how consistently you are able to meet challenges
that appear at various stages of the game. When you are a beginner, you might find it impossible to make it to level 4. When you are a little
more experienced, you will consistently make it to level 5. Every once in a while you'll do something stupid and die on level 2, but
*on average*, you will make much more progress than ever seemed possible when you were just starting out.

With this in mind: what is it that modern roguelike-influenced games get wrong about roguelike design?

*Teleglitch*, a self-described action roguelike, was released in 2013 to critical acclaim.
The critical praise cited sparse, evocative writing and an oppressive atmosphere engendered by spooky sound design, minimalist graphics and high difficulty. Agreed!
The game was not, however, a good roguelike.

![teleglitch](http://i.imgur.com/ezPGCvj.jpg)

Let's review actions that the player can take at any given moment in *Teleglitch*.

* Attack
* Move
* Use items
* Combine items

Very similar to *Rogue*, no? But here's the problem. Items are scarce in *Teleglitch*. Ridiculously scarce, at times. You need ammo for your
weapons, because your default knife attack is too weak and close-range to use against most enemies in the game.
But ammo is scarce to the point where you can have pinpoint-perfect shot accuracy and still run out of ammo. I died over and over playing *Teleglitch*. I made mistakes.
I ran out into the open before scouting for enemies. I turned my pistol into a nailgun before I had found any nails. But most of the time it was because
there just weren't any resources. The game forces you to explore, but punishes you for doing so by hurling enemies at you without replenishing the costs of fighting them.
At some point, I realized I wasn't really learning anything anymore. It was more like playing a lottery, where the prize was to not
be mercilessly doomed by resource scarcity. Here's what one successful Teleglitch speedrunner has to say about the game:

> A lot of thought and planning went into it, but in the end a lot is going to be determined by RNG.
> Does a Motor drop early, do you miss out on health along the way and die due to that.
> If monsters drop zero ammo, you're in big trouble. So many things that can go wrong.

To reiterate my point: Roguelikes aren't interesting simply because they're hard, or random. That's not enough to make something a good roguelike.
Roguelikes are interesting because they're about *learning*.
It's about the thrill of using knowledge and experience to overcome great difficulty.
It's about using your preparation to anticipate dangerous situations.
It's about knowing that when you died, it was because you weren't prepared enough yet.
The Roman Stoic philosopher Seneca said: "Luck is what happens when preparation meets opportunity." I think this idea is at the core of all great roguelikes.
After all, what is the effect of having randomly-varying level design, instead of static design? It forces the player to abstract
their approach to preparation -- they can't simply say "oh, I know a Yeti is going to show up here, so I'd better be ready for that." The player
must prepare themselves for a wide range of situations that could arise at any moment. This is what makes roguelikes so difficult, and so interesting to play.
If your game is functionally impossible to win on a large percentage of attempts, that makes it very difficult for your player to understand
what they are doing wrong, and how to better prepare themselves. I think this is a misunderstanding of what makes roguelikes such an interesting genre.

*But, Evan, isn't it possible that you could get a really unlucky run in Shiren the Wanderer?*

Yes, it definitely is possible to have a run where you are only given access to weak items, or very little food. But these situations are the exception,
not the rule. Most of the time it's your poor planning and suboptimal choices that spell your doom. To give one example, many players who complain about things like
running out of food aren't exploring the floor efficiently, causing them to waste lots of food. This leads to an impression that resources are scarce, when in actuality they
are simply not being used properly. *Teleglitch*, on the other hand, forces you to accept that even if you play perfectly, you will likely still run out of resources and lose the game.
(To be clear: I enjoyed playing *Teleglitch* and think it's a very interesting game. I just don't think it's a good roguelike. Survival horror maybe?)

*Why wouldn't this criticism apply to a game like [Dwarf Fortress](http://www.bay12games.com/dwarves/)?*

For one thing, *Dwarf Fortress* does not have a goal state, so on a long enough timescale failure is always inevitable. But again I think we need to return to the concept of
*amusingly complex failure*. In a game with amusingly complex failure, failure for experienced players is often not the result of simple rules, i.e. "when the player touches an enemy, they take damage",
but rather of an unforeseen chain of events which leads to the player's downfall. Let's take a look at this *Dwarf Fortress* player's story:

> I once had my militia commander killed by a piece of liver. Let me be specific, I had closed my gates while trading with a human caravan.
> When the trading was finished and the traders were ready to leave, I designated the gate to be opened and turned my focus to my various other projects.
> Well unfortunately for me, the order was cancelled and I failed to notice. After what must have been a month of imprisonment, one of the merchants finally snapped, he grabbed the closest item to him, a piece of sheep liver and threw it as hard as he could at my militia commander, slamming him against the wall, splitting his head open, and killing him instantly.
> In one brutal attack, this merchant had killed my militia commander, a legendary Axedwarf who had single handedly destroyed four goblin invasions, who was loved by every dwarf in the fort and was clad entirely in iron armor and wielded a artifact steel battleaxe and he did this right in front of my meeting hall.
> The resulting riot destroyed my fort in under five minutes.

In this story, a seemingly inconsequential action - accidentally trapping a merchant caravan inside the fortress - triggers a sequence of events
that unravels a stable fortress. Sound familiar to the *Shiren* story above? *Dwarf Fortress* is unique, however, in that players take
great delight in experiencing new and unforeseen ways to catastrophically fail. For some, sharing these stories is the most compelling reason to play.
Compare this to my last three runs of *Teleglitch*.

> 1) I ran out of ammo, and died.
> 2) I blew myself up with a bomb.
> 3) I ran out of ammo, and died.

The complexity of rules and possible interactions between elements is simply much lower in *Teleglitch* than in most roguelikes.
I had similar issues with indie darling *Rogue Legacy*, released two years ago.
*Rogue Legacy* is an action-platformer which features randomized level layouts and tough-to-beat enemies... but whenever I lost, it was for similar reasons.
*I jumped into a bat. A ghost shot ice magic at me.* There just... wasn't anything funny about it, no catastrophic chain reactions that caught me totally off guard or surprised me.
It's more of a *Castlevania*-like, in that it demands you to memorize enemy movement and attack patterns and utilize quick reflexes to avoid them.
This is difficult, yes, but not in the same way that roguelikes are difficult. I love *Castlevania*. But let's not confuse it for *Rogue*. In the end, I probably enjoyed *Rogue Legacy*
less than I might have if it had not labeled itself as a roguelike, because it gave me expectations about the game that it was uninterested in fulfilling.

*Spelunky*, in contrast, managed to successfully translate the spirit of the roguelike to the platforming genre.

![spelunky](http://i.imgur.com/lYYXUaK.jpg)

Here's a story Tom Francis (the mind behind the wonderful *Gunpoint*) tells:

> I buy a shotgun from the top floor, then drop down to blow all the tribesmen away in one shot. I miss. A boomerang knocks me out of the air, nailbitingly close to a fatal pitcher plant below, and onto the snail. The snail is crushed, but the tribesmen are wild: by the time I pick myself up, one has thrown himself to his death and the other has jumped into the shops. Now he prowls them slowly, looking for me.
> This is tense. I’m dying to shoot him, but it’s madness to fire in the direction of a shopkeeper. I just have to tail him at a safe distance and buy the items I need as I pass them. I’m reasonably confident he won’t turn round – and even if he does, he dropped his boomerang outside.
> There’s a boomerang on sale in this shop actually. The tribesman walks up to it. He picks it up.
> For a split second, I am amused. He’s going to buy a new boomerang! Silly tribesman, you don’t own material wealth!
> Then my internal simulation of Spelunky’s interacting systems kicks in, and I see the next few seconds flash before my eyes with pure horror.
> I run.
> I jump onto the ladder, scramble up, dive away from the top floor shops, duck behind a mound of earth and hug the ground. Jesus Jesus Jesus Jesus Christ.
> For a second, nothing happens.
> Then the Black Market explodes.
> All nine shopkeepers hurl themselves into the air and start firing their shotguns in random directions. They kill the tribesman. They kill two other tribesmen. They kill frogs, pitchers, snails. One kills the slave he was selling, another kills his own dog. Two of them throw themselves to their deaths in the excitement. Four of them throw themselves into a pit, where their bursts of buckshot cut each other to ribbons.
> When the blasts quiet down, I crawl slowly out of my hiding place and walk carefully through the empty shops, collecting everything for free. What happened here was: the Tribesman walked out of the shop. He walked out of the shop with the shopkeeper’s boomerang in his hand, and he walked out of the shop without paying for it.
> Shopkeepers don’t know, much less understand, who stole from them or damaged their store. Any crime, of any kind, is cause for an indiscriminate rampage that kills everything in line of sight, and a lot more besides. When that happens in the Black Market, there’s a term for it. It’s the shopstorm.

Shopkeepers sure cause a lot of chaos don't they?
Here we see, again, that simple sets of rules are able to interact with each other to form surprisingly complex behaviors. We also see that Tom was able to use his knowledge of the ruleset to
anticipate and avoid a situation that would have surely resulted in explosive failure.

It's easy to make a game that stomps on your players.
Making a game that allows them to learn from their mistakes, producing both failures which are
educational and amusing, and hard-won triumphant successes... now there's something truly difficult.
