---
layout: post
title:  "Dev Update - The Story So Far"
date:   2015-03-11 18:45:00
category: Devlog
tags: [Heretic Sun, flavor]
---

A few months ago I began working on a tactics RPG.

The concept:

* playthrough takes a reasonable (YMMV) length of time (8-10 hours)
* procedural generation (characters, maps, etc.) for replayability
* difficult enough that victory is not a guaranteed prospect
* no restarting or save scumming - players must face the consequences of their choices
* emergent behavior based on character traits

After giving the project some thought, I decided that the grid-based combat system was going to require the most work, and I should finish that first.

I figured that the best approach would be to first get map generation working to the point that I could reasonably test the battle system,
and then work on the battle system until it was basically feature-complete before moving back to procedural generation.

I've now implemented a large chunk of the battle system logic, to the point where I can play through
a simple battle with (unbalanced and underdeveloped) characters.

When I get bored of dealing with technical under-the-hood stuff I like to add little flavor features to keep
development interesting. For the last day or so I've been working on speech bubbles:

![pay no attention to the borrowed sprites](http://fat.gfycat.com/BlackSickFerret.gif)

The characters now yell things based on what action is being performed, which class they belong to, and their...
for lack of a better word, "personality" (More on this later...) I haven't finished all the
text but it already adds some nice flavor to the battles.
